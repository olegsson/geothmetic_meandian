# Geothmetic meandian

Pure Python implementation of the [geothmetic meandian](https://xkcd.com/2435/), as formalized by Randall Munroe (2021).

# Install

`pip install git+https://gitlab.com/olegsson/geothmetic_meandian.git`

# Usage

```
In [1]: from geothmetic_meandian import gmdn
In [2]: gmdn([1, 1, 2, 3, 5], precision=4)
Out[2]: 2.0891
```
