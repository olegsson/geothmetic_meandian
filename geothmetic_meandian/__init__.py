import statistics as st
from typing import Iterable

__version__ = '0.0.1'

def gmdn(
    data: Iterable,
    *,
    precision: int=10,
):
    mean, gmean, median = (
        round(st.__dict__[f](data), precision)
        for f in (
            'mean',
            'geometric_mean',
            'median',
        )
    )
    if mean == gmean == median:
        return mean
    return gmdn([mean, gmean, median], precision=precision)
